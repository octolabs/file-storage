FROM node:lts-alpine  AS builder
WORKDIR /app
COPY package*.json ./
RUN  npm install
COPY . .
RUN npm run build

FROM node:lts-alpine  AS final
WORKDIR /app
USER node
ADD --chown=node package.json .
COPY --from=builder --chown=node /app/dist ./dist
COPY --from=builder --chown=node /app/node_modules ./node_modules
EXPOSE 3000
CMD ["npm", "run", "start:prod"]
