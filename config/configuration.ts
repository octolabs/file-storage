export default () => ({
  port: process.env.PORT || 3000,
  minio: {
    endpoint: process.env.MINIO_ENDPOINT,
    port: parseInt(process.env.MINIO_PORT),
    accessKey: process.env.MINIO_ACCESS_KEY,
    secretKey: process.env.MINIO_SECRET_KEY,
    useSSL: process.env.MINIO_USE_SSL === 'true' ? true : false,
  },
  mongo: {
    uri: process.env.MONGO_URI,
  },
});
