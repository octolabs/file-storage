import {
  BadRequestException,
  Controller,
  Get,
  Param,
  Post,
  Response,
  UploadedFile,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import * as express from 'express';
import { FileStorageService } from '../core/application/file-storage.service';

@Controller('files')
export class FileStorageController {
  constructor(private fileStorageService: FileStorageService) {}

  @Post()
  @UseInterceptors(FileInterceptor('file'))
  async uploadFile(@UploadedFile() file) {
    if (!file || !file.buffer) throw new BadRequestException('invalid file');
    return this.fileStorageService.save({
      fileStream: file.buffer,
      filename: file.originalname,
      mimeType: file.mimetype,
    });
  }

  @Get(':id')
  async findById(@Param('id') id: string) {
    return this.fileStorageService.getMetaData(id);
  }

  @Get(':id/download')
  async download(
    @Param('id') id: string,
    @Response() response: express.Response,
  ) {
    return (await this.fileStorageService.get(id)).pipe(response);
  }
}
