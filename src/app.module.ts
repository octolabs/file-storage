import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MongooseModule } from '@nestjs/mongoose';
import configuration from 'config/configuration';
import { FileStorageController } from './api/app.controller';
import { FileStorageService } from './core/application/file-storage.service';
import { FileMeta, FileSchema } from './infrastructure/file.schema';
import { MinioService } from './infrastructure/minio.service';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: [configuration],
    }),

    MongooseModule.forRootAsync({
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => ({
        uri: configService.get('mongo.uri'),
      }),
    }),
    MongooseModule.forFeature([{ name: FileMeta.name, schema: FileSchema }]),
  ],
  controllers: [FileStorageController],
  providers: [FileStorageService, MinioService],
})
export class AppModule {}
