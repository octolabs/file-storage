import { Injectable, Scope } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { FileMeta, FileDocument } from '../../infrastructure/file.schema';
import { MinioService } from '../../infrastructure/minio.service';

@Injectable({ scope: Scope.REQUEST })
export class FileStorageService {
  private bucket: string;
  constructor(
    configService: ConfigService,
    private minioService: MinioService,
    @InjectModel(FileMeta.name) private fileModel: Model<FileDocument>,
  ) {
    this.bucket = configService.get('MINIO_BUCKET');
  }

  async save({
    filename,
    fileStream,
    mimeType,
  }: {
    filename: string;
    fileStream;
    mimeType: string;
  }): Promise<string> {
    await this.createBucketIfNotExists();
    const extension = /(?:\.([^.]+))?$/.exec(filename)[1];
    const fileMeta = new this.fileModel({
      name: filename,
      extension,
    });

    const savedFileMeta = await fileMeta.save();

    const generatedFileName = `${savedFileMeta._id}.${extension}`;
    await this.minioService.putObject(
      this.bucket,
      generatedFileName,
      fileStream,
      {
        'Content-Type': mimeType,
      },
    );
    return savedFileMeta._id;
  }

  async get(id: string): Promise<any> {
    const foundFileMeta = await this.fileModel.findById(id).exec();
    return this.minioService.getObject(
      this.bucket,
      `${foundFileMeta._id}.${foundFileMeta.extension}`,
    );
  }

  async getMetaData(id: string): Promise<FileDocument> {
    return this.fileModel.findById(id).exec();
  }

  async delete(id: string): Promise<void> {
    const foundFileMeta = await this.fileModel.findById(id).exec();
    await this.minioService.removeObject(
      this.bucket,
      `${foundFileMeta._id}.${foundFileMeta.extension}`,
    );
    await this.fileModel.deleteOne({ _id: id }).exec();
    return;
  }

  private async createBucketIfNotExists() {
    try {
      await this.minioService.makeBucket(this.bucket, 'europe');
    } catch (e) {
      if (e.code === 'BucketAlreadyOwnedByYou') {
        return;
      }
      throw e;
    }
  }
}
