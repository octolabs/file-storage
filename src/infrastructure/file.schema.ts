import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

export type FileDocument = FileMeta & Document;

@Schema()
export class FileMeta {
  @Prop()
  id: string;

  @Prop()
  extension: string;

  @Prop()
  name: string;

  @Prop({
    default: new Date(),
  })
  creationDate: Date;

  @Prop()
  path: string;
}

export const FileSchema = SchemaFactory.createForClass(FileMeta);
