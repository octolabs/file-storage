import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';

import { Client } from 'minio';
@Injectable()
export class MinioService extends Client {
  constructor(configService: ConfigService) {
    super({
      endPoint: configService.get('minio.endpoint'),
      port: configService.get('minio.port'),
      accessKey: configService.get('minio.accessKey'),
      secretKey: configService.get('minio.secretKey'),
      useSSL: configService.get('minio.useSSL'),
    });
  }
}
